package com.goherepal;

public class App {
	
	private String name;
	private int id;
	private int scrambler;
	
	public int generateKeyValue() {
		return this.id * this.scrambler;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getScrambler() {
		return scrambler;
	}
	
	public void setScrambler(int scrambler) {
		this.scrambler = scrambler;
	}
	
	
	
}
