package com.goherepal;

public class AppLock {
	
	private int keyValue;
	private boolean lockVar;
	
	public AppLock(int keyValue) {
		this.keyValue = keyValue;
		lockVar = true;
	}
	
	public boolean unlock(int keyValue) {
		if(this.keyValue == keyValue) {
			lockVar = false;
			return true;	
		}
		lockVar = true;
		return false;
	}
	
	public void lock() {
		lockVar = true;
	}
	
	public boolean isLocked() {
		return lockVar;
	}
}
