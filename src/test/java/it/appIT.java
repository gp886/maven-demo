package it;

import static org.junit.Assert.*;

import org.junit.Test;

import com.goherepal.App;
import com.goherepal.AppLock;

public class appIT {

	@Test
	public void IntegrationKeyTest() {
		App appVar = new App();
		int testId =101;
		String testName = "Rohit";
		int testScrambler = 97;
		
		appVar.setId(testId);
		appVar.setName(testName);
		appVar.setScrambler(testScrambler);
	
		AppLock appLockVar = new AppLock(testId*testScrambler);
		
		assertEquals("App variable unable to open lock! Test Failed.",true,appLockVar.unlock(appVar.generateKeyValue()));
		assertEquals("Lock is not open.Test Failed",false,appLockVar.isLocked());
		
		appLockVar.lock();
		appVar.setScrambler(testScrambler-4);
		
		assertEquals("App variable able to open lock with wrong keyvalue! Test Failed.",false,appLockVar.unlock(appVar.generateKeyValue()));
		assertEquals("Lock is open.Test Failed",true,appLockVar.isLocked());
		
		
	}

}
