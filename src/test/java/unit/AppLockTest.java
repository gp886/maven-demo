package unit;

import static org.junit.Assert.*;

import org.junit.Test;

import com.goherepal.AppLock;

public class AppLockTest {

	@Test
	public void test() {
		/*fail("Not yet implemented");*/
		int trueTestKV = 12345;
		int falseTestKV = 54321;
				
		AppLock objVar = new AppLock(trueTestKV);
		
		assertEquals("Initial isLocked test failed",true,objVar.isLocked()); //Initial Lock Status//
		
		assertEquals("False KV Unlock test failed",false,objVar.unlock(falseTestKV)); //Unlocking through false keyValue//
		
		assertEquals("isLocked test after false attempt",true,objVar.isLocked()); //Checking status of lock after false attempt//
		
		assertEquals("True KV Unlock test failed",true,objVar.unlock(trueTestKV));//Unlocking through true key-value //
		
		assertEquals("isLocked test after true attempt",false,objVar.isLocked()); //Checking status of lock after true attempt//
	
		objVar.lock();
		
		assertEquals("isLocked test after locking failed",true,objVar.isLocked()); //Checking status of lock after true attempt//
			
	}

}
