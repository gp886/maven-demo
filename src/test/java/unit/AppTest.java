package unit;

import static org.junit.Assert.*;

import org.junit.Test;

import com.goherepal.App;

public class AppTest {

	@Test
	public void test() {
		App objVar = new App();
		int testId =142;
		String testName = "Rohit";
		int testScrambler = 3;
		
		objVar.setId(testId);
		objVar.setName(testName);
		objVar.setScrambler(testScrambler);
		
		assertEquals("Testing in App.java failed",objVar.generateKeyValue(), testId*testScrambler);
		
	}

}
